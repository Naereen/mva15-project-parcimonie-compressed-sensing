## List of useful slides from [Tro14]

### 35
Eq (7) Mirsky's Theorem (1969)
Slide 35
[Tro14]

### 42
Total Costs for Truncated SVD
Slide 42

### 52
Prototype for Randomized Range Finder
Slide 52
plein dexplications

### 59
Deterministic Error Bound for Range Finder
Slide 59
2 refs pour le theorem

### 63
Subsampled randomized Fourier transform (SRFT)
Slide 63
joli dessin. F -> mathcal{F}

### 68
Randomized Range Finder + Power Scheme
Slide 68
role du petit q

### Fin
Simple Error Bound for Power Scheme for the Randomized Range Finder
Theorem 8.
[HMT 2011, §.10]

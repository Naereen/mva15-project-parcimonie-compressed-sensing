function O = SFRT(n, l)
% O = SFRT(n, l)
%   Key function of my implementation, implementing the SFRT algorithm.
%   More details on my project report.
%
%   Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
%   Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
%   Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
%   Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
%   Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
%   MIT license (http://lbesson.mit-license.org/).
%
    % R matrix
    R = zeros(n, l);
    o = 1:l;
    for c = 1:l
        u = rand;
        i = ceil(size(o, 2)*u);
        r = o(i);
        R(r, c) = 1;
        o = [o(1:i-1), o(i+1:size(o, 2))];
    end
    % D matrix, random values on the complex circle
    theta = 2*pi*rand(1, n);
    D = exp(- 1j * theta);
    % n x n complex Rademacher diagonal matrix
    D = diag(D);
    % F matrix
    F = dftmtx(n);  % from the communications package, NxN FFT
    O = sqrt(n/l) * D * F * R;
end

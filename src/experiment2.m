% Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
% Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
% Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
% Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
% Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
% MIT license (http://lbesson.mit-license.org/).

disp('');
disp('« Probabilistic Algorithms for Constructing Approximate Matrix Decompositions » experiment #2');
disp('See http://lbo.k.vu/pcs2016 for the report and more');
disp('Course: Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) for the Master MVA');
disp('Author: 2015-16, Lilian Besson (http://perso.crans.org/besson/)');
disp('');
disp('This software is provided “as is”, without warranty of any kind.');
disp('Please read the LICENSE file or MIT license (http://lbesson.mit-license.org/) for more details.');


%% Add the toolbox, download them from the Internet if needed
disp('0. Loading toolboxes ...');
disp('   If it fails, install them from http://www.numerical-tours.com/installation_matlab/');
% http://www.numerical-tours.com/installation_matlab/
getd = @(p) path(p, path);
% http://www.numerical-tours.com/matlab/toolbox_signal.zip
disp('  Loading toolbox_signal/ ...');
getd('toolbox_signal/');
% http://www.numerical-tours.com/matlab/toolbox_general.zip
disp('  Loading toolbox_general/ ...');
getd('toolbox_general/');

disp('  Loading package communications ...');
pkg load communications;  % Required for dftmtx


%% Parameters
disp('1. Initializing parameter of the simulation ...');

% We try the same code on other images, just to be safe
name = 'lena'
% name = 'boat'
% name = 'mandrill'
disp(['Using the reference image ' name]);


% Sub image size. We tried larger, OK.
imagesize = 40
disp(['  Using a sub-image of ' name ', of size ' num2str(imagesize) ' pixels × ' num2str(imagesize) ' pixels.']);

% Patch size. We tried larger, OK.
cropsize = 5
disp(['  And patches of small size ' num2str(cropsize) ' pixels × ' num2str(cropsize) ' pixels.']);

% sigma similarity parameter (cf. GML course by Michal Valko)
% http://researchers.lille.inria.fr/~valko/hp/mva-ml-graphs
% We tried to change it, it does not affect much the final result.
sigma = 50
disp(['  Using a similarity parameter sigma = ' num2str(sigma) '.']);

% Control sparsity of the Wtilde final matrix
nbsparse = 7

row_size = imagesize - cropsize + 1;
disp(['  Forcing every row of the similarity graph matrix to be s = ' num2str(nbsparse) ' sparse (rows of size = ' num2str(row_size) ').']);
disp(['  This is a sparsity of ' num2str(100 * row_size / imagesize) ' %.']);


%% Loading image

disp('2. Loading the image ...');
disp('   If it fails, check that toolbox_signal/ is available from the current directory.')
% image = load_image(name);  % If the new line fails
image = load_image(['toolbox_signal/' name]);
% Octave weird thing: has to import it twice
image = load_image(['toolbox_signal/' name]);

fullsize_x = size(image, 1);
fullsize_y = size(image, 2);
disp(['  Displaying this image to a size ' num2str(fullsize_x) ' pixels × ' num2str(fullsize_y) ' pixels ...']);
figure();clf;
imageplot(image);
colorbar('EastOutside');
title(['Full test image (' name '), ' num2str(fullsize_x) 'x' num2str(fullsize_y) ' pixels']);

% Rescale the image
disp(['  Rescaling and displaying this image to a size ' num2str(imagesize) ' pixels × ' num2str(imagesize) ' pixels ...']);
image = rescale(crop(image, imagesize));
figure();clf;
imageplot(image);
colorbar('EastOutside');
title(['Small test sub-image (' name '), ' num2str(imagesize) 'x' num2str(imagesize) ' pixels']);


%% Data Matrix
disp('3. Creating the data matrix A.')

tic;
disp(['  Creating the patches, of size ' num2str(cropsize) ' ...']);
[X, M] = PatchVector(image, cropsize);
disp(['  1-D vector of all the patches, has size ' num2str(size(X)) ' .']);
disp(['  2-D matrix of all the patches, has size ' num2str(size(M)) ' .']);
toc;
M_averaged = mean(M, 3);
figure(); clf;
imageplot(M_averaged);
colorbar('EastOutside');
title('Average values of the patches, displayed as an image.');

tic;
disp('  Creating the similarity graph matrix Wtild (it should take about 5 seconds) ...');
Wtild = WeightMatrix(image, sigma, cropsize);
size_Wtild_x = size(Wtild, 1);
size_Wtild_y = size(Wtild, 2);
disp(['  2-D graph adjacency matrix Wtild, has size ' num2str(size_Wtild_x) 'x' num2str(size_Wtild_x) ' .']);
toc;

% % XXX spy is useless here, Wtild is not sparse (non-zero on each coefficient !)
% figure(); clf;
% spy(Wtild);
% title('Full similarity graph weight matrix Wtild (from the ' name ' image)');

disp('  Displaying this graph matrix as an image ...');
figure(); clf;
imageplot(Wtild);  % From the toolbox_signal
% imshow(Wtild);  % XXX use this line if the previous fails
colorbar('EastOutside');
title(['Full similarity graph weight matrix Wtild (from the ' name ' image), size = ' num2str(size_Wtild_x) 'x' num2str(size_Wtild_x)]);


tic;
disp(['  Creating a sparse similarity graph matrix W, keeping only the ' num2str(nbsparse) ' highest values on each row (it should take about 5 seconds) ...']);
% W = SparseWeightMatrix(image, sigma, cropsize, nbsparse);
W = SparseWeightMatrix(Wtild, nbsparse);
disp(['  2-D graph adjacency matrix W, has size ' num2str(size(W)) ' .']);
toc;

% Evaluating sparsity
predicted_sparsity = (nbsparse) / sqrt(prod(size(W)))
evaluated_sparsity = sum(sum(W != 0)) / prod(size(W))
disp(['  W has a sparsity about ' num2str(100 * evaluated_sparsity) ' % and the predicted sparsity was ' num2str(nbsparse) ' / ' num2str(sqrt(prod(size(W)))) ' = ' num2str(100 * predicted_sparsity) ' % !']);

disp('  Displaying this sparse graph matrix (with spy) ...');
figure(); clf;
spy(W);
title(['Sparse weight matrix W (from the ' name ' image), s = ' num2str(100 * evaluated_sparsity) ' %']);


% XXX Experimental : goes to a *true* sparse data-structure (memory efficient)
Wsparse = sparse(W);
% W = Wsparse;


tic;
disp(['  Creating the sparse Laplacian A (it should take about 0.35 seconds) ...']);
A = AuxilaryMatrix(W);
% XXX Experimental : goes to a *true* sparse data-structure (memory efficient)
% A = sparse(A);
size(A)
toc;

% Now A should be of size 1296 × 1296, very sparse
disp(['  Displaying this graph Laplacian matrix as an image, has size ' num2str(size(A)) ' ...']);
figure(); clf;
spy(A);
title(['Sparse graph Laplacian matrix A (from the ' name ' image), s = ' num2str(100 * evaluated_sparsity) ' %']);


%% 3 different stage A, and always same stage B
% input('3. Demonstration of different stage A algorithms ...  [Type Enter to continue]');

disp('3. Demonstration of different stage A algorithms ...');
disp('  A. Experimenting with the RandomizedRangeFinder q=0 and q=4, FastRandomizedRangeFinder algorithms for stage A.');
disp('  B. Using the DirectSVD algorithm for stage B.');

% XXX goes back to 100 !
l = 100
disp([' Using a length l = ' num2str(l) ' ...']);


q = 0
disp('3.1. First version of the randomized range finder, q = 0 ...');
disp('     It should take about 43 seconds ...');  % XXX too slow!
tic;
Q = RandomizedRangeFinder(A, l, q);  % Stage A !
tic;
toc;  % No reason the count the stage B
% [S, U, V] = DirectSVD(A, Q);  % Stage B !
[S, U, V] = QuickDirectSVD(A, Q, l);  % Quicker Stage B !
toc;
s1 = diag(S);

figure(); clf;
plot(1:l, s1(1:l), 'b+-');
xlabel('Index i');
ylabel('i-th Singular Value');
title(['Randomized Range Finder with q=0 (on the ' name ' image), decaying singular values']);


q = 4
disp(['3.2. Second version of the randomized range finder, Randomized Power Iteration q = ' num2str(q) ' ...']);
disp('     It should take about 63 seconds ...');  % XXX too slow!
tic
Q = RandomizedRangeFinder(A, l, q);  % Stage A !
tic;
toc;  % No reason the count the stage B
% [S, U, V] = DirectSVD(A, Q);  % Stage B !
[S, U, V] = QuickDirectSVD(A, Q, l);  % Quicker Stage B !
toc;
s2 = diag(S);

figure(); clf;
plot(1:l, s2(1:l), 'r*-');
xlabel('Index i');
ylabel('i-th Singular Value');
title(['Randomized Power Iteration with q=4 (on the ' name ' image), decaying singular values']);


disp('3.3. Fast randomized range finder ...');
disp('     It should take about 108 seconds ...');  % XXX too slow!
tic;
Q = FastRandomizedRangeFinder(A, l);  % Stage A !
tic;
toc;  % No reason the count the stage B
[S, U, V] = DirectSVD(A, Q);  % Stage B !
% [S, U, V] = QuickDirectSVD(A, Q, l);  % Quicker Stage B !
toc;
s3 = diag(S);

figure(); clf;
plot(1:l, s3(1:l), 'gs-');
xlabel('Index i');
ylabel('i-th Singular Value');
title(['Fast Randomized Range Finder (on the ' name ' image), decaying singular values']);


% Make a final plot, showing the three one the same image
figure(); clf;
hold('on');

subplot(131);
plot(1:l, s1(1:l), 'b+-');
xlabel('Index i');
ylabel('i-th Singular Value');
legend('Randomized Range Finder (q=0)');

subplot(132);
plot(1:l, s2(1:l), 'r*-');
xlabel('Index i');
ylabel('i-th Singular Value');
legend('Randomized Power Iteration (q=4)');
title(['Comparison of the 3 stage-A algorithms (on the ' name ' image), decaying singular values']);

subplot(133);
plot(1:l, s3(1:l), 'gs-');
xlabel('Index i');
ylabel('i-th Singular Value');
legend('Fast Randomized Range Finder');
hold('off');


% End
disp('')
disp('You can save the figures if needed.')
input('[Press Enter to finish the simulation]');
disp('End of the simulation.')

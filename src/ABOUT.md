% Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
% Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
% Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
% Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
% Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
% MIT license (http://lbesson.mit-license.org/).


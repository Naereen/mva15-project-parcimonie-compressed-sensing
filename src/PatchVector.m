function [X, M] = PatchVector(image, cropsize)
% [X, M] = PatchVector(image, cropsize)
%   Compute a patch vector of the image, to size cropsize.
%   Inspiration from https://github.com/NelleV/COPRE/blob/master/src/data.py
%   Create Patch based matrices P and M
%   P: Contains each patch as a column vector
%   M: Each element of the 2D matrix is a patch
%
%   Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
%   Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
%   Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
%   Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
%   Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
%   MIT license (http://lbesson.mit-license.org/).
%
    x = size(image,1);
    y = size(image,2);
    fullimagesize = x * y;

    patchVectorSize = cropsize^2;
    c = floor(cropsize/2);
    % Calculating the number of patches;
    % Image locations in the bottom-right corner do not have a patch
    N = (x-2*c) * (y-2*c);

    % each column of X is a patch
    X = zeros(N, patchVectorSize);  % Limited memory
    % X = zeros(fullimagesize, patchVectorSize);

    % some locations of M corresponding to the bottom right corner of the image have to be neglected
    M = zeros(x-2*c, y-2*c, patchVectorSize);  % Limited memory
    % M = zeros(x, y, patchVectorSize);

    k=0;
    for i = c+1:x-c
        for j = c+1:y-c
            % getting the patch whose center is pixel(i,j) of the image
            onePatchMatrix = image(i-c:i+c,j-c:j+c);
            onePatchVector = reshape(onePatchMatrix,[],1);
            k = k+1;
            X(k, :) = onePatchVector;
            M(i-c, j-c, :) = onePatchVector;
        end
    end
end

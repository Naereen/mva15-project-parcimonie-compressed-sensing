# [*Parcimonie and Compressed Sensing*](http://gpeyre.github.io/teaching/) project (Master MVA)
### Meta-data:
- *Subject*: **Probabilistic Algorithms for Constructing Approximate Matrix Decompositions**,
- *Advisor*: [Gabriel Peyré](http://gpeyre.github.io/teaching/),
- *Keywords*: QR, SVD, probabilistic matrix decomposition algorithms,
- *Where*: [git repository on bitbucket.org](https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing).

### Feedback:
- **Grade**: I got 19/20 for this course,
- **Rank**: 1st amongst 36 students who got a grade (for them, average was 14.19/20). Initially, 57 students were registered on the course,
- I presented [the slides](./slides/) at a [lab meeting](http://bigwww.epfl.ch/people.html?photo=2015) at the [BIG (LIB) team](http://bigwww.epfl.ch/) at [EPLF, Lausanne, Switzerland](http://www.epfl.ch/) the 08-02, and [some of them](http://bigwww.epfl.ch/storath/index.html) were quite enthusiastic about it!

----

## Project reports and slides
### [Final research report](./report/)
> - 10-15 pages. It was due February 1st (2016),

See [here for the (final) PDF report](https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing/downloads/MVA_Parcimonie_Compressed_Sensing__project__Lilian_Besson__2015-16__Report.en.pdf).

### [Final presentation](./slides/) (slides)
> - I wrote a 13-slide PDF presentation,
> - **Only 6 minutes** for the presentation!
> - The presentation was on February 1st (at 16:00 for me).

See [here for the (final) PDF slides](https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing/downloads/MVA_Parcimonie_Compressed_Sensing__project__Lilian_Besson__2015-16__Slides.en.pdf).

----

## Topic
> - « P28 - Probabilistic algorithms for constructing approximate matrix decompositions »
> - [Abstract](http://arxiv.org/abs/0909.4061),
> - [Main reference in PDF](http://users.cms.caltech.edu/~jtropp/papers/HMT11-Finding-Structure-SIREV.pdf).

### [Description](http://arxiv.org/abs/0909.4061):
> *Low-rank matrix approximations, such as the truncated singular value decomposition and the rank-revealing QR decomposition, play a central role in data analysis and scientific computing. This work surveys and extends recent research which demonstrates that randomization offers a powerful tool for performing low-rank matrix approximation. These techniques exploit modern computational architectures more fully than classical methods and open the possibility of dealing with truly massive data sets.*
> *For this project, I studied a modular framework for constructing randomized algorithms that compute partial matrix decompositions. These methods use random sampling to identify a subspace that captures most of the action of a matrix. The input matrix is then compressed -- either explicitly or implicitly -- to this subspace, and the reduced matrix is manipulated deterministically to obtain the desired low-rank factorization. In many cases, this approach beats its classical competitors in terms of accuracy, speed, and robustness. These claims are supported by extensive numerical experiments and a detailed error analysis.*

----

## TODO list (DONE)
### Main dates
- [X] 25th January: first draft of [the report](./report/) and [the slides](./slides),
- [X] 1st of February: **[final report](./report)** and **[slides](./slides)** ready.

### [Reading papers](./biblio/)
- [X] I read the [main references](./biblio/mva15-project-parcimonie-compressed-sensing.pdf),
- [X] OK, I am keeping [the BibTeX file up-to-date](./biblio/mva15-project-parcimonie-compressed-sensing.bib).

### [Code](./src/) (in Octave/Matlab)
> See [the README about code](./src/README.md), and [the requirements](./src/requirements.txt).

- [X] A [simple algorithm for "stage 1"](./src/RandomizedRangeFinder.m),
- [X] A [simple algorithm for "stage 2"](./src/DirectSVD.m),
- [X] A simple plumbing around the two, a [small simulation](./src/experiment1.m),
- [X] One or two ["stage 1" algorithms](FastRandomizedRangeFinder.m),
- [X] A better "stage 2" algorithm ? Nope, they only present structure-specific post-processing algorithms.
- [X] A [better simulation to experiment more](./src/experiment2.m).

### [Theory](./report/) (OK)
- [X] Study the convergence/bound proofs from the paper,
- [X] For the report, add the minimum material on linear algebra. The report should be "self-contained" for the main results,
- [X] Work on every proof.

----

## About
This project was done for the [*Parcimonie and Compressed Sensing*](http://gpeyre.github.io/teaching/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

> [Contributors](./contributors.txt) | [A few git statistics](./complete-stats.txt) | [Git Makefile](./Makefile)

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project has been publicly published the 31-01-16, under the terms of the [MIT license](http://lbesson.mit-license.org/).

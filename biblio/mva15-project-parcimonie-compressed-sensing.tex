% Parcimonie and Compressed Sensing (http://gpeyre.github.io/teaching/) project (Master MVA)
% Subject: Probabilistic algorithms for constructing approximate matrix decompositions,
% Advisor: Gabriel Peyré (http://gpeyre.github.io/teaching/) (Gabriel.Peyre at ceremade.dauphine.fr),
% Where: git repository on bitbucket.org (https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing),
% Author: (C), 2015-16, Lilian Besson (http://perso.crans.org/besson/)
% MIT license (http://lbesson.mit-license.org/).

\documentclass[onecolumn,11pt,a4paper]{article}
% This first part of the file is called the PREAMBLE. It includes
% customizations and command definitions. The preamble is everything
% between \documentclass and \begin{document}.

%% Well understand input, and nice output
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}

\usepackage[margin=0.85in,top=1.35in,bottom=1.35in]{geometry}  % set the margins, for two columns
\usepackage{latexsym}              % symbols
\usepackage{amsmath}               % great math stuff
\usepackage{amssymb}               % great math symbols
\usepackage{MnSymbol,amsbsy}
\usepackage{amsfonts}              % for blackboard bold, etc
\usepackage{amsthm}                % for theorems, http://tex.stackexchange.com/a/130655

\usepackage{enumerate}
\usepackage{fixltx2e} % LaTeX patches, \textsubscript
\usepackage{cmap} % fix search and cut-and-paste in Acrobat
\usepackage{color}

\usepackage[plainpages=false,pdfcenterwindow=true,
    pdftoolbar=false,pdfmenubar=false,
    pdftitle={Final Project Report - Sparsity and Compressed Sensing - Master MVA},
    pdfauthor={Lilian Besson (ENS Cachan)},
    linkcolor=black,citecolor=black,filecolor=black,urlcolor=black]{hyperref}
     % Allows url with \href{http://truc.machin/ok?r=yes}{myref}
\usepackage{graphicx}              % to include figures
\usepackage{caption}
\usepackage{subcaption}
\usepackage{multicol}
\usepackage{float}
\usepackage{framed}
\usepackage{palatino} % Use the Palatino font % XXX
\usepackage{enumitem}  % For \begin{enumerate}, switch back to itemize if it fails

% Various theorems, numbered by section
\newtheorem{theorem}{Theorem}[section]
% \newtheorem{proof}[theorem]{Proof}  % Already defined by amsthm
\newtheorem{remark}[theorem]{Remark}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{corollary}[theorem]{Corollary}

\usepackage{lastpage,fancyhdr}
\pagestyle{fancy}
    \renewcommand{\headrulewidth}{0.2pt}
    \renewcommand{\footrulewidth}{0.2pt}
    \lhead{\emph{Project Report -- Sparsity and Compressed Sensing course}}
    \rhead{\emph{\today}}
    \lfoot{Master MVA -- ENS Cachan}
    \cfoot{$\thepage/\pageref{LastPage}$}
    \rfoot{\href{http://perso.crans.org/besson/}{Lilian~~Besson}}

%% Horizontal Lines
\providecommand*{\hr}[1][class-arg]{%
    \hspace*{\fill}\hrulefill\hspace*{\fill}
    \vskip 0.65\baselineskip
}

\newcommand{\range}{\mathrm{range}}
\newcommand{\eqdef}{\mathrel{\stackrel{\smash{\scriptscriptstyle\mathrm{def}}}{=}}}

% \usepackage{fancybox}


% XXX if needed, switch back to these simpler metadata ?
% \author{Lilian Besson}
% \title{Finding Structure with Randomness: Probabilistic Algorithms for Approximate Matrix Decompositions}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\DeclareGraphicsExtensions{.pdf,.png,.jpg}

\large  % XXX for the first page

% \setcounter{LastPage}{-1}  %% XXX
% \setcounter{page}{0}  %% XXX


% ------------------------------------------------------------------------------
\title{\Huge{\textbf{Finding Structure with Randomness}}\\ \Large{Probabilistic Algorithms for Approximate Matrix Decompositions} \\ \vspace*{2pt} \Large{Research Project Report -- ``Sparsity and Compressed Sensing'' course} }

% TODO: Cleanup and publish on my web-page the report, and add it as a preprint on arXiv ?
% TODO: and on GitHub/Bitbucket the code and experiments (and add it a DOI via Zenodo ?)

\thispagestyle{plain}

\date{}
% \date{\today}
\maketitle


\vspace*{-2pt}
% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space

% ------------------------------------------------------------------------------
% Authors
\begin{center}
        \setcounter{footnote}{-1}  %% XXX
        \href{http://perso.crans.org/besson/}{Lilian~Besson}\footnote{ If needed, see on-line at \url{http://lbo.k.vu/pcs2016} for an e-version of this report, as well as additional resources (slides, code, figures, complete bibliography etc), open-sourced under the \href{http://lbesson.mit-license.org/}{MIT License}.} \\
        Department of Mathematics\\
        \'Ecole Normale Sup\'erieure de Cachan (France) \\
        \texttt{{lilian}{.}{besson}{{@}}{ens-cachan}{.}{fr}}
\end{center}


% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space

% ------------------------------------------------------------------------------
% A small abstract of what is done in the paper
\begin{normalsize}
\begin{abstract}
    \begin{normalsize}

    Matrix factorization is a powerful tool to achieve tasks efficiently
    in \href{https://en.wikipedia.org/wiki/Numerical_linear_algebra}{numerical linear algebra}.
    A problem arises when we compute low-rank approximations for massive matrices:
    we have to reduce the algorithms' complexity in time to hope to be efficient.
    A way to adapt these techniques for such computational environments is randomization.
    This report presents a framework for these new techniques, based mainly on the work
    by N.~Halko, P.-G.~Martinsson and J.~Tropp
    presented in
    ``Finding Structure with Randomness: Probabilistic Algorithms for Constructing approximate Matrix Decompositions'' \cite{HMT11,Tropp14-Structure}.

    The main intuition behind the various algorithms presented herein is
    using random sampling to apprehend the action of the matrix in a ``compressed'' subspace.
    We can apply then the classical methods on the resulting matrix
    -- the one acting on the ``compressed'' subspace --
    to obtain a low-rank approximation.
    Another application that rises from this explanation is the fact
    that this method is thus more robust addressing incomplete data sets
    that one can get in information sciences, while other.
    The benefits of such methods will depend on the matrix.

    We will decline the results for three main classes:
        %
        \emph{dense matrices} where the new complexity is about $O(m n \log(k))$,
        compared to $O(m n k)$ for classical methods -- $k$ being the numerical rank;
        %
        \emph{sparse matrices}, and in general, structured matrices;
        %
        \emph{massive matrices} that do not fit in fast memory (\emph{RAM}) for which the access time
        -- which surpasses computation time --
        can be reduced to one pass, comparing to $O(k)$ passes.

    A clean implementation was developed, and we will present 2 experiments,
    done on both dense and sparse matrices, in order to confirm the theoretical analysis.
    % FIXME really, j'ai eu le temps de faire l'exp 1 ?

    \end{normalsize}
\end{abstract}
\end{normalsize}


% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of Contents - One page ?!
\newpage

% ------------------------------------------------------------------------------
% About the report
\begin{center}
    \begin{large}
        \textbf{Project Advisor:} \href{https://gpeyre.github.io/}{Gabriel Peyré} (\href{https://www.ceremade.dauphine.fr/}{CEREMADE}, \href{http://www.dauphine.fr/}{Paris-Dauphine University}) \\
        \textbf{Course:}
        \emph{\href{https://gpeyre.github.io/teaching}{``Sparsity and Compressed Sensing''}}, by \href{https://gpeyre.github.io/}{G. Peyré}, in $2015$--$2016$ \\
        \textbf{Master $2$ program: } \href{http://www.math.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp}{Math\'ematiques, Vision, Apprentissage (MVA)}
        at \href{http://www.ens-cachan.fr/}{ENS de Cachan}. \\
        \textbf{Grade:} I got $\textcolor{red}{X} / 20$ for my project (not yet graded).
    \end{large}
\end{center}

% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space


\appendix
% ------------------------------------------------------------------------------
\section{Appendix}
\label{sec:appendix}

% ------------------------------------------------------------------------------
\subsection{Acknowledgments}
\label{sub:acknowledgments}

    I would like to thank \href{https://gpeyre.github.io/}{Gabriel Peyré} my project advisor,
    as he replied quickly to my few queries and provided useful direction of research.
    %
    % Thanks also to my MVA comrade Basile Clement for a quick proofreading of my project report.

% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space

%% Bibliography
\subsection{References}
\label{sub:references}

% Remove the Reference title
% Idea from http://tex.stackexchange.com/a/22654
\renewcommand{\section}[2]{}%
%\renewcommand{\chapter}[2]{}% for other classes
% \renewcommand{\bibname}{\subsubsection*{References}} % Redefine bibname

% \nocite{*}  % XXX remove to hide references not cited!
% \phantom{\cite{XXX}}
\phantom{\cite{AF07, Albert72, Cipra00, CooleyTukey65, Courrieu08, Dongarra00, Duhamel90, GE96, Givoly01, Grivet13, HMT11, JL84, numerical-tours, Rakha04, Tropp12-Tool-art, Tropp12-Tool-slides, Tropp14-Structure, WLRT08}}

% To compile: pdflatex, pdflatex, bibtex, pdflatex, pdflatex
% \bibliographystyle{alpha}
\bibliographystyle{naereen}  % XXX use alpha if any issue occurs with my custom theme

% \bibliography{../main}  % Symlinks!
\bibliography{../biblio/mva15-project-parcimonie-compressed-sensing}
    \begin{center}
        (\emph{Note:} a more detailed bibliography is available \href{https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing/src/master/biblio/}{on-line}, in HTML and in BibTeX.)
    \end{center}


% ------------------------------------------------------------------------------
\hr{}  % Horizontal line, like <hr> in HTML
% XXX remove if it is ugly or take too much space


\vfill
% ------------------------------------------------------------------------------
\subsection*{License?}
\label{sub:license}
    % \begin{center}
        % \begin{small}
            This paper (and the additional resources -- including code, poster images, etc)
            are \href{https://bitbucket.org/lbesson/mva15-project-parcimonie-compressed-sensing/}{publicly published} under the terms of the \href{http://lbesson.mit-license.org/}{MIT License}.
            %
            Copyright 2015-2016, \copyright ~Lilian~Besson.

            % Note that this article has \textbf{not} been published
            % on any conference, journal or pre-print platform.
            % It was just the result of a small research Master project.
        % \end{small}
    % \end{center}


\end{document}

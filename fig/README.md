## Figures
- These figures have been generated with [GNU Octave](http://octave.org/), with [this program](../src/main.m),
- The figures are used in [the report](../report/) and [the slides](../slides/).

#### More ?
- See [the report](../report/) for explanations about these figures.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)
